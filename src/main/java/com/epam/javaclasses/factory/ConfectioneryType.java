package com.epam.javaclasses.factory;

public enum ConfectioneryType {
    CAKE,
    CANDY,
    COOKIE
}
