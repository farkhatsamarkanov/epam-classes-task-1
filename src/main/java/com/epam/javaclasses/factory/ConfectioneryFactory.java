package com.epam.javaclasses.factory;

import com.epam.javaclasses.entity.*;

public class ConfectioneryFactory {
    public static Confectionery getConfectioneryFromFactory(ConfectioneryType type) {
        switch (type) {
            case CAKE:
                return new Cake("Chocolate cake", 2.5, 2.0, "cake", 350, "sour");
            case CANDY:
                return new Candy("Nut candy", 1.32, 0.025, "candy", 100, "nuts");
            case COOKIE:
                return new Cookie("Fortune cookie", 1.01, 0.03, "cookie", 55.5, "paper");
            default:
                throw new IllegalArgumentException("Illegal type of confectionery!");
        }
    }
}
