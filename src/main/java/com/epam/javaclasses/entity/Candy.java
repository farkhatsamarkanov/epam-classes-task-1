package com.epam.javaclasses.entity;

import java.util.Objects;

public class Candy extends Confectionery {
    private String filling;                                             // Class fields

    public Candy(String name, double cost, double weight, String type, double gramsOfSugar, String filling) {  // Constructor
        super(name, cost, weight, type, gramsOfSugar);
        this.filling = filling;
    }

    public String getFilling() {                                        // Getters
        return filling;
    }

    public void setFilling(String filling) {                            // Setters
        this.filling = filling;
    }

    @Override                                                            // Overriding parent method
    public String cook() {
        return "Candy is being cooked";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Candy)) return false;
        Candy candy = (Candy) o;
        return Objects.equals(candy.getFilling(), getFilling()) &&
                Double.compare(candy.getCost(), getCost()) == 0 &&
                Double.compare(candy.getWeight(), getWeight()) == 0 &&
                Double.compare(candy.getGramsOfSugar(), getGramsOfSugar()) == 0 &&
                Objects.equals(candy.getName(), getName()) &&
                Objects.equals(candy.getType(), getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFilling(), getName(), getCost(), getWeight(), getType(), getGramsOfSugar());
    }

}
