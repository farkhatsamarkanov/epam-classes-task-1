package com.epam.javaclasses.entity;

import java.util.Objects;

public class Cake extends Confectionery {
    private String typeOfCream;                                     // Class fields

    public Cake(String name, double cost, double weight, String type, double gramsOfSugar, String typeOfCream) {  // Constructor
        super(name, cost, weight, type, gramsOfSugar);
        this.typeOfCream = typeOfCream;
    }

    public String getTypeOfCream() {                                // Getters
        return typeOfCream;
    }

    public void setTypeOfCream(String typeOfCream) {               // Setters
        this.typeOfCream = typeOfCream;
    }

    @Override                                                       // Overriding parent method
    public String cook() {
        return "Cake is being cooked";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cake)) return false;
        Cake cake = (Cake) o;
        return Objects.equals(cake.getTypeOfCream(), getTypeOfCream()) &&
                Double.compare(cake.getCost(), getCost()) == 0 &&
                Double.compare(cake.getWeight(), getWeight()) == 0 &&
                Double.compare(cake.getGramsOfSugar(), getGramsOfSugar()) == 0 &&
                Objects.equals(cake.getName(), getName()) &&
                Objects.equals(cake.getType(), getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTypeOfCream(), getName(), getCost(), getWeight(), getType(), getGramsOfSugar());
    }
}
