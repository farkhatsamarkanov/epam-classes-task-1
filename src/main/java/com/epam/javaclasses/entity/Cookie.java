package com.epam.javaclasses.entity;

import java.util.Objects;

public class Cookie extends  Confectionery {
    private String flavour;                                         // Class fields

    public Cookie(String name, double cost, double weight, String type, double gramsOfSugar, String flavour) {   // Constructor
        super(name, cost, weight, type, gramsOfSugar);
        this.flavour = flavour;
    }

    public String getFlavour() {                                    // Getters
        return flavour;
    }

    public void setFlavour(String flavour) {                        // Setters
        this.flavour = flavour;
    }

    @Override                                                        // Overriding parent method
    public String cook() {
        return "Cookie is being cooked";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cookie)) return false;
        Cookie candy = (Cookie) o;
        return Objects.equals(candy.getFlavour(), getFlavour()) &&
                Double.compare(candy.getCost(), getCost()) == 0 &&
                Double.compare(candy.getWeight(), getWeight()) == 0 &&
                Double.compare(candy.getGramsOfSugar(), getGramsOfSugar()) == 0 &&
                Objects.equals(candy.getName(), getName()) &&
                Objects.equals(candy.getType(), getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlavour(), getName(), getCost(), getWeight(), getType(), getGramsOfSugar());
    }

}
