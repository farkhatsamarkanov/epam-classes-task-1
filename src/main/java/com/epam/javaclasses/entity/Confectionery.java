package com.epam.javaclasses.entity;

public abstract class Confectionery {                    // An abstract parent class for all confectioneries in a gift box
    private String name;                                  // Class fields
    private double cost;
    private double weight;
    private String type;
    private double gramsOfSugar;

    public Confectionery(String name, double cost, double weight, String type, double gramsOfSugar) { // Constructor
        this.name = name;
        this.cost = cost;
        this.weight = weight;
        this.gramsOfSugar = gramsOfSugar;
        this.type = type;
    }

    public String getName() {                             // Getters
        return name;
    }

    public double getCost() {
        return cost;
    }

    public double getWeight() {
        return weight;
    }

    public String getType() {
        return type;
    }

    public double getGramsOfSugar() {
        return gramsOfSugar;
    }

    public void setName(String name) {                    // Setters
        this.name = name;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setGramsOfSugar(double gramsOfSugar) {
        this.gramsOfSugar = gramsOfSugar;
    }

    public abstract String cook();                          // Methods

    @Override
    public String toString() {
        return "Confectionery{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", weight=" + weight +
                ", type='" + type + '\'' +
                ", gramsOfSugar='" + gramsOfSugar + '\'' +
                '}';
    }
}
