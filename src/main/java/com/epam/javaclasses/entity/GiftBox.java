package com.epam.javaclasses.entity;

import com.epam.javaclasses.factory.*;
import com.epam.javaclasses.comparator.ComparatorCriterion;
import java.util.*;

public class GiftBox {
    private List<Confectionery> giftBoxContents;                                             // List containing confectioneries
    private StringBuilder log = new StringBuilder("com.epam.javaclasses.entity.GiftBox\n"); // Logger

    public List<Confectionery> getGiftBoxContents() {                                        // Getters
        return giftBoxContents;
    }

    public StringBuilder getLog() {
        return log.append("\n");
    }
                                                                                              // Class methods
    public void assembleGiftBox(int numberOfGifts) {                                         // Method chooses confectioneries randomly from enum and adds them to gift box (param: number of gifts to add)
        Set<Confectionery> filterSet = new HashSet<>();                                       // Using HashSet to get rid of duplicates
        for (int i = 0; i < numberOfGifts; i++) {
            ConfectioneryType type = ConfectioneryType.values()[new Random().nextInt(ConfectioneryType.values().length)];
            Confectionery confectionery = ConfectioneryFactory.getConfectioneryFromFactory(type);  // Getting random confectionery
            if (filterSet.add(confectionery)) {                                                    // If there is no same confectionery in a gift box, then add
                log.append(confectionery.cook())
                        .append(" and added to gift box\n");
            } else {
                log.append(type)
                        .append(" is already in the gift box!\n");
            }
        }
        giftBoxContents = new ArrayList<>(filterSet);
        log.append("Gift box assembled!\n\n");
    }

    public void printGiftTotalCostAndWeight() {                                    // Method to print total cost and weight of a gift box
        double totalCost = 0.0;
        double totalWeight = 0.0;
        for (Confectionery confectionery : giftBoxContents) {                       // Iterating through gifts and getting cost and weight
            totalCost += confectionery.getCost();
            totalWeight += confectionery.getWeight();
        }
        log.append("Total cost of a gift: ")
           .append(totalCost)
           .append(" $\n");
        log.append("Total weight of a gift: ")
           .append(totalWeight)
           .append(" g\n\n");
        System.out.println("Total cost of a gift: " + totalCost + " $");
        System.out.println("Total weight of a gift: " + totalWeight + " g");
        System.out.println();
    }

    public void sortGiftBoxContentsBy(ComparatorCriterion criterion) {                  // Method sorts contents of a gift box by some criterion
        switch (criterion) {
            case NAME:
                giftBoxContents.sort(Comparator.comparing(Confectionery::getName));      // Using comparator to compare confectioneries to each other
                break;                                                                   // Using Collections.sort to sort confectioneries
            case COST:
                giftBoxContents.sort(Comparator.comparingDouble(Confectionery::getCost));
                break;
            case GRAMS_OF_SUGAR:
                giftBoxContents.sort(Comparator.comparingDouble(Confectionery::getGramsOfSugar));
                break;
            case WEIGHT:
                giftBoxContents.sort(Comparator.comparingDouble(Confectionery::getWeight));
                break;
            case TYPE:
                giftBoxContents.sort(Comparator.comparing(Confectionery::getType));
                break;
            default:
                log.append("Illegal sorting criterion!\n\n");
                throw new IllegalArgumentException("Illegal sorting criterion!");
        }
        log.append("Sorting gift box contents by criterion: ")
                .append(criterion)
                .append("\n\n");
    }

    public void printConfectioneryWithNumericalParameter (ComparatorCriterion criterion, double rangeMinValue, double rangeMaxValue) { // Method to find confectionery with specific parameter value
        List<Confectionery> toReturn = new ArrayList<>();
        if (rangeMinValue <= rangeMaxValue) {
            for (Confectionery confectionery : giftBoxContents) {
                double parameterToCompare;
                switch (criterion) {
                    case WEIGHT:
                        parameterToCompare = confectionery.getWeight();
                        break;
                    case COST:
                        parameterToCompare = confectionery.getCost();
                        break;
                    case GRAMS_OF_SUGAR:
                        parameterToCompare = confectionery.getGramsOfSugar();
                        break;
                    default:
                        throw new IllegalArgumentException("Illegal criterion!");
                }
                if (parameterToCompare > rangeMinValue && parameterToCompare < rangeMaxValue) {     // Checking if parameter is in right value
                    toReturn.add(confectionery);
                }
            }
            if (toReturn.isEmpty()) {
                log.append("Unfortunately, there is no sweet with necessary parameter!")
                   .append("\n\n");
                System.out.println("Unfortunately, there is no sweet with necessary parameter!");
            } else {
                log.append("There is(are) ")
                   .append(toReturn.size())
                   .append(" sweet(s) that has(ve) necessary parameter ")
                   .append(criterion)
                   .append(" (")
                   .append(rangeMinValue)
                   .append(" - ")
                   .append(rangeMaxValue)
                   .append("):\n");
                System.out.println("There is(are) " + toReturn.size() + " sweet(s) that has(ve) necessary parameter " + criterion + " (" + rangeMinValue + " - " + rangeMaxValue + "):");
                printGiftBoxContents(toReturn);
            }
        } else {
            log.append("Minimum range value cannot be bigger than maximum range value!")
               .append("\n\n");
            throw new IllegalArgumentException("Minimum range value cannot be bigger than maximum range value!");
        }
    }


    public void printGiftBoxContents(List<Confectionery> giftBoxContents) { // Printing each confectionery
        for (Confectionery confectionery : giftBoxContents) {
            System.out.println(confectionery);
            log.append(confectionery)
               .append("\n");
        }
        System.out.println();
        log.append("\n");
    }
}
