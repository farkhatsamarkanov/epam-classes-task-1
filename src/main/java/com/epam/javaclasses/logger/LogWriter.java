package com.epam.javaclasses.logger;

import com.epam.javaclasses.entity.GiftBox;
import java.io.*;

public class LogWriter {                // Class to write log file using PrintWriter
    public static void writeGiftAssemblingLog(String fileName, GiftBox giftBox) {
        File file = new File(fileName);
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        PrintWriter printWriter = null;
        try{
            if (file.createNewFile()) {
                System.out.println("Log file created!");
            } else {
                System.out.println("Log file already exists!");
            }
            fileWriter = new FileWriter(file, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            printWriter.println(giftBox.getLog());                      // Writing log to a file
            System.out.println("Log is written!");
        }catch (IOException e){
            System.err.println("Error while opening file: " + e);
        }finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }
    }
}
