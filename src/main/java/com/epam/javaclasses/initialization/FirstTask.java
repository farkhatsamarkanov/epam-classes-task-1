package com.epam.javaclasses.initialization;

import com.epam.javaclasses.comparator.ComparatorCriterion;
import com.epam.javaclasses.entity.GiftBox;
import com.epam.javaclasses.logger.LogWriter;

public class FirstTask {
    public static void main (String[] args) {
        GiftBox giftBox = new GiftBox();                    // Creating gift box
        giftBox.assembleGiftBox(10);            // Trying to add 10 confectioneries to a gift box
        System.out.println("Initial gift box contents:");
        giftBox.printGiftBoxContents(giftBox.getGiftBoxContents());
        giftBox.printGiftTotalCostAndWeight();              // Printing total weight and cost of a gift box
        giftBox.sortGiftBoxContentsBy(ComparatorCriterion.GRAMS_OF_SUGAR);
        System.out.println("Gift box contents after sorting by amount of sugar:");
        giftBox.printGiftBoxContents(giftBox.getGiftBoxContents());
        giftBox.printConfectioneryWithNumericalParameter(ComparatorCriterion.GRAMS_OF_SUGAR, 200, 500);
        LogWriter.writeGiftAssemblingLog("log.txt", giftBox);
    }
}
