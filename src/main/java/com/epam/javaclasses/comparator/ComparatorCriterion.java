package com.epam.javaclasses.comparator;

public enum ComparatorCriterion {   //enumeration of criteria for sorting or finding specified confectionery
    NAME,
    COST,
    WEIGHT,
    GRAMS_OF_SUGAR,
    TYPE
}
